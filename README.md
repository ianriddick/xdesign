# Munro Library Challenge

## Requirements

For building and running the application you will need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)


## Running the tests

To execute the tests locally

```shell
mvn test
```

## Running the application locally

to run the application locally use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html):

```shell
mvn spring-boot:run
```

# API reference 

Once the application is running you can access the API via a GET request to return a list of Munro Records.
The API has several query parameters which are all optional - if no parameters are specified then all data will return 

##Parameters

* max-records - limit the results returned - this is applied after the filter and sorting has been applied to return the top x
* hill-category - filter records by hill category to filter on - either MUN or TOP
* min-height - filter records with a height greater than this value
* max-height - filter records with a height lower than this value
* sort-field - sort records by field either HEIGHT or NAME
* sort-direction - sort direction either ASC or DESC

## Example
### Example Request 
The example below will return the top 5 records with a height greater than 940 but less than 2000 sorted ascending order by name

```shell
http://localhost:8080/api/munro?max-records=5&min-height=940&max-height=2000&sort-field=name&sort-direction=ASC
```
### Example Response 

```shell
[
    {
        "name": "A' Chailleach",
        "height": 997,
        "gridRef": "NH136714",
        "munroType": "MUN"
    },
    {
        "name": "A' Chraileag [A' Chralaig]",
        "height": 1120,
        "gridRef": "NH094147",
        "munroType": "MUN"
    },
    {
        "name": "A' Chralaig - A' Chioch",
        "height": 947,
        "gridRef": "NH108152",
        "munroType": "TOP"
    },
    {
        "name": "A' Chralaig - Stob Coire na Craileig [Stob Coire na Cralaig] [Mullach Fraoch-choire S Top]",
        "height": 1008,
        "gridRef": "NH091163",
        "munroType": "TOP"
    },
    {
        "name": "A' Mhaighdean",
        "height": 967,
        "gridRef": "NH007749",
        "munroType": "MUN"
    }
]
```



