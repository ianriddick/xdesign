package com.xdesign.munro.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.xdesign.munro.dto.Direction;
import com.xdesign.munro.dto.Munro;
import com.xdesign.munro.dto.MunroType;
import com.xdesign.munro.dto.QueryRequest;
import com.xdesign.munro.dto.Sort;
import com.xdesign.munro.dto.SortField;
import com.xdesign.munro.entity.MunroEntity;
import com.xdesign.munro.repository.MunroRepository;

/**
 * unit test for the {@link RepositoryMunroService }
 */
class RepositoryMunroServiceTest {

	private MunroRepository mockMunroRepository;

	private RepositoryMunroService target;

	@BeforeEach
	void beforeTest () {
		mockMunroRepository = Mockito.mock(MunroRepository.class);
		target = new RepositoryMunroService(mockMunroRepository);
	}
	
	@Test
	void shouldReturnAllRecordsExcludingMunroTypeNullGivenEmptyQueryRequest () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntity("M1",MunroType.MUN),
				givenMunroEntity("M2",MunroType.MUN),
				givenMunroEntity("M3",null),
				givenMunroEntity("M4",null),
				givenMunroEntity("M5",MunroType.MUN)
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).extracting("name").containsExactlyInAnyOrder("M1","M2","M5");
		
	}

	@Test
	void shouldFilterByMunroTypeGivenQueryRequestMunroTypeTop () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntity("M1",MunroType.MUN),
				givenMunroEntity("M2",MunroType.TOP),
				givenMunroEntity("M3",null),
				givenMunroEntity("M4",null),
				givenMunroEntity("M5",MunroType.MUN)
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withHillCategory(MunroType.TOP).build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).extracting("name").containsExactlyInAnyOrder("M2");
	}

	@Test
	void shouldFilterByMunroTypeGivenQueryRequestMunroTypeMun () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntity("M1",MunroType.MUN),
				givenMunroEntity("M2",MunroType.TOP),
				givenMunroEntity("M3",null),
				givenMunroEntity("M4",null),
				givenMunroEntity("M5",MunroType.MUN)
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withHillCategory(MunroType.MUN).build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).extracting("name").containsExactlyInAnyOrder("M1","M5");
	}
	
	@Test
	void shouldFilterByMunroTypeGivenQueryRequestMunroTypeNull () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntity("M1",MunroType.MUN),
				givenMunroEntity("M2",MunroType.TOP),
				givenMunroEntity("M3",MunroType.MUN)
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withHillCategory(null).build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).extracting("name").containsExactlyInAnyOrder("M1","M2","M3");
	}
	
	@Test
	void shouldReturnFilteredGivenRequestHeightLessthan10 () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntityWithHeight("M1","20.99"),
				givenMunroEntityWithHeight("M2","2.9"),
				givenMunroEntityWithHeight("M3","10.01"),
				givenMunroEntityWithHeight("M4","10.00"),
				givenMunroEntityWithHeight("M5","20.99")
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withMaxHeight(BigDecimal.TEN).build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).extracting("name").containsExactlyInAnyOrder("M2","M4");
		
	}

	
	@Test
	void shouldReturnSortedGivenRequestHeightSortedAsc () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntityWithHeight("M1","20.99"),
				givenMunroEntityWithHeight("M2","2.9"),
				givenMunroEntityWithHeight("M3","10.01"),
				givenMunroEntityWithHeight("M4","10.00"),
				givenMunroEntityWithHeight("M5","20.98")
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withSort(Arrays.asList(
				Sort.builder().withField(SortField.HEIGHT).withDirection(Direction.ASC).build()
				)).build();

		// When
		List<Munro> result = target.searchMunroData(request);

		// Then
		assertThat(result).extracting("name").containsExactly("M2","M4","M3","M5","M1");
		
	}
	
	@Test
	void shouldReturnSortedGivenRequestHeightSortedDesc () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntityWithHeight("M1","20.99"),
				givenMunroEntityWithHeight("M2","2.9"),
				givenMunroEntityWithHeight("M3","10.01"),
				givenMunroEntityWithHeight("M4","10.00"),
				givenMunroEntityWithHeight("M5","20.98")
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withSort(Arrays.asList(
				Sort.builder().withField(SortField.HEIGHT).withDirection(Direction.DESC).build()
				)).build();

		// When
		List<Munro> result = target.searchMunroData(request);


		// Then
		assertThat(result).extracting("name").containsExactly("M1","M5","M3","M4","M2");
		
	}
	
	@Test
	void shouldReturnSortedGivenRequestNameSortedAsc () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntity("Stob Binnein",MunroType.MUN),
				givenMunroEntity("Stob Binnein - Stob Coire an Lochain",MunroType.MUN),
				givenMunroEntity("Stob Binnein - Creag a' Bhragit",MunroType.MUN),
				givenMunroEntity("Stob Binnein - Meall na Dige",MunroType.MUN),
				givenMunroEntity("Cruach Ardrain SW Top",MunroType.MUN)
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withSort(Arrays.asList(
				Sort.builder().withField(SortField.NAME).withDirection(Direction.ASC).build()
				)).build();

		// When
		List<Munro> result = target.searchMunroData(request);

		// Then
		assertThat(result).extracting("name").containsExactly(
				"Cruach Ardrain SW Top",
				"Stob Binnein",
				"Stob Binnein - Creag a' Bhragit",
				"Stob Binnein - Meall na Dige",
				"Stob Binnein - Stob Coire an Lochain");

	}
	
	@Test
	void shouldReturnSortedGivenRequestNameSortedDesc () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntity("Stob Binnein",MunroType.MUN),
				givenMunroEntity("Stob Binnein - Stob Coire an Lochain",MunroType.MUN),
				givenMunroEntity("Stob Binnein - Creag a' Bhragit",MunroType.MUN),
				givenMunroEntity("Stob Binnein - Meall na Dige",MunroType.MUN),
				givenMunroEntity("Cruach Ardrain SW Top",MunroType.MUN)
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withSort(Arrays.asList(
				Sort.builder().withField(SortField.NAME).withDirection(Direction.DESC).build()
				)).build();

		// When
		List<Munro> result = target.searchMunroData(request);

		// Then
		assertThat(result).extracting("name").containsExactly(
				"Stob Binnein - Stob Coire an Lochain",
				"Stob Binnein - Meall na Dige",
				"Stob Binnein - Creag a' Bhragit",
				"Stob Binnein",
				"Cruach Ardrain SW Top"
				);
		
	}
	
	
	@Test
	void shouldReturnSortedGivenMultipleSortFields () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntityWithHeight("Stob Binnein","10.00"),
				givenMunroEntityWithHeight("Stob Binnein - Stob Coire an Lochain","10.00"),
				givenMunroEntityWithHeight("Stob Binnein - Creag a' Bhragit","10.00"),
				givenMunroEntityWithHeight("Stob Binnein - Meall na Dige","10.00"),
				givenMunroEntityWithHeight("Cruach Ardrain SW Top","9.99")
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withSort(Arrays.asList(
				Sort.builder().withField(SortField.HEIGHT).withDirection(Direction.ASC).build(),
				Sort.builder().withField(SortField.NAME).withDirection(Direction.DESC).build()
				)).build();

		// When
		List<Munro> result = target.searchMunroData(request);

		// Then
		assertThat(result).extracting("name").containsExactly(
				"Cruach Ardrain SW Top",
				"Stob Binnein - Stob Coire an Lochain",
				"Stob Binnein - Meall na Dige",
				"Stob Binnein - Creag a' Bhragit",
				"Stob Binnein"
				);

	}
	
	@Test
	void shouldReturnFilteredGivenRequestHeightGreaterthan10 () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntityWithHeight("M1","20.99"),
				givenMunroEntityWithHeight("M2","2.9"),
				givenMunroEntityWithHeight("M3","10.01"),
				givenMunroEntityWithHeight("M4","10.00"),
				givenMunroEntityWithHeight("M5","20.99")
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withMinHeight(BigDecimal.TEN).build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).extracting("name").containsExactlyInAnyOrder("M1","M3","M4","M5");
	}

	@Test
	void shouldFilterAndReturnNoFieldsGivenRequestSpecifiesMaxHeightZero () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntityWithHeight("M1","20.99"),
				givenMunroEntityWithHeight("M2","2.9"),
				givenMunroEntityWithHeight("M3","10.01"),
				givenMunroEntityWithHeight("M4","10.00"),
				givenMunroEntityWithHeight("M5","20.99")
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withMaxHeight(BigDecimal.ZERO).build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).isEmpty();
		
	}
	
	@Test
	void shouldReturnAllRecordsAndLimitResultGivenQueryRequestLimit () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntity("M1",MunroType.MUN),
				givenMunroEntity("M2",MunroType.MUN),
				givenMunroEntity("M3",MunroType.MUN),
				givenMunroEntity("M4",MunroType.MUN),
				givenMunroEntity("M5",MunroType.MUN)
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withLimit(3).build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).extracting("name").containsExactlyInAnyOrder("M1","M2","M3");
		
	}
	
	
	@Test
	void shouldReturnTopRecordsAndLimitResultGivenQueryRequestLimitAndSorted () {
		// Given
		Set<MunroEntity> data = givenDataSet (
				givenMunroEntity("A",MunroType.MUN),
				givenMunroEntity("B",MunroType.TOP),
				givenMunroEntity("C",MunroType.MUN),
				givenMunroEntity("D",MunroType.TOP),
				givenMunroEntity("E",MunroType.TOP)
				);
		when(mockMunroRepository.findAll()).thenReturn(data);
		target.preloadData();
		QueryRequest request = QueryRequest.builder().withHillCategory(MunroType.TOP).withSort(
				Arrays.asList(Sort.builder().withField(SortField.NAME).withDirection(Direction.ASC).build())
				).withLimit(2).build();

		// When
		List<Munro> result = target.searchMunroData(request);
		
		// Then
		assertThat(result).extracting("name").containsExactlyInAnyOrder("B","D");
	}
	

	private Set<MunroEntity> givenDataSet(MunroEntity ... entities) {
		// give the entities the record index as per file in the order that they were created
		long index = 1;
		for ( MunroEntity entity : entities) {
			entity.setIndex(index++);
		}
		return new HashSet<>(Arrays.asList(entities));
	}

	private MunroEntity givenMunroEntity( String name, MunroType munroType) {
		return MunroEntity.builder().withName(name).withMunroType(munroType).build();
	}
	
	private MunroEntity givenMunroEntityWithHeight( String name, String height) {
		return MunroEntity.builder().withName(name).withHeight(new BigDecimal(height)).withMunroType(MunroType.MUN).build();
	}
	
}
