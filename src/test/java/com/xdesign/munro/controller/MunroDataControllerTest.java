package com.xdesign.munro.controller;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.xdesign.munro.controller.exception.MunroControllerBadRequestException;
import com.xdesign.munro.dto.Direction;
import com.xdesign.munro.dto.Munro;
import com.xdesign.munro.dto.MunroType;
import com.xdesign.munro.dto.QueryRequest;
import com.xdesign.munro.dto.Sort;
import com.xdesign.munro.dto.SortField;
import com.xdesign.munro.service.MunroService;

/**
 * Unit test class for the {@link MunroDataController} 
 */
class MunroDataControllerTest {

	private MunroDataController target;
	private MunroService mockMunroService;
	

	@BeforeEach
	private void init () {
		mockMunroService = Mockito.mock(MunroService.class);
		target = new MunroDataController(mockMunroService);
	}
	
	@Test
	void shouldCallServiceWithNoParamatersSupplied () {
		// Given
		Integer limitBy = null;
		String hillCategory = null;
		String maxHeight= null;
		String minHeight= null;
		String sortField= null;
		String sortDirection= null;
		QueryRequest expectedQueryRequest = QueryRequest.builder().withSort(Collections.emptyList()).build();
		List<Munro> expectedReturn = Collections.emptyList();
		when(mockMunroService.searchMunroData(expectedQueryRequest)).thenReturn(expectedReturn);
		
		// When
		List<Munro> result = target.munroFilter(limitBy, hillCategory, maxHeight, minHeight, sortField, sortDirection);
				
		// Then
		assertThat(result).isEqualTo(expectedReturn);
		verify(mockMunroService).searchMunroData(expectedQueryRequest);
	}
	
	
	@Test
	void shouldCallServiceWithAllParamatersSetGivenSuppliedValues () {
		// Given
		Integer limitBy = 10;
		String hillCategory = "TOP";
		String maxHeight= "90";
		String minHeight= "10";
		String sortField= "height";
		String sortDirection= "ASC";
		List<Sort> expectedSort = Arrays.asList(Sort.builder().withField(SortField.HEIGHT).withDirection(Direction.ASC).build());
		QueryRequest expectedQueryRequest = QueryRequest.builder()
				.withLimit(limitBy)
				.withHillCategory(MunroType.TOP)
				.withMaxHeight(new BigDecimal(maxHeight))
				.withMinHeight(new BigDecimal(minHeight))
				.withSort(expectedSort)
				.build();
		List<Munro> expectedReturn = Collections.emptyList();
		when(mockMunroService.searchMunroData(expectedQueryRequest)).thenReturn(expectedReturn);
		
		// When
		List<Munro> result = target.munroFilter(limitBy, hillCategory, maxHeight, minHeight, sortField, sortDirection);
				
		// Then
		assertThat(result).isEqualTo(expectedReturn);
		verify(mockMunroService).searchMunroData(expectedQueryRequest);
	}
	
	@Test
	void shouldFailToCallCallServiceGivenInvalidHillCategory () {
		// Given
		Integer limitBy = null;
		String hillCategory = "XXX";
		String maxHeight= null;
		String minHeight= null;
		String sortField= null;
		String sortDirection= null;
		
		// When
		MunroControllerBadRequestException thrown = assertThrows(
				MunroControllerBadRequestException.class,
		           () ->  target.munroFilter(limitBy, hillCategory, maxHeight, minHeight, sortField, sortDirection),
		           "Expected munroFilter() to throw an MunroControllerBadRequestException"
		    );
		// Then
		assertThat(thrown).hasMessage("Munro type XXX is invalid - valid types are [MUN, TOP]");
		
	}
	
	@Test
	void shouldFailToCallCallServiceGivenInvalidHMaxHeight () {
		// Given
		Integer limitBy = null;
		String hillCategory = null;
		String maxHeight= "FR";
		String minHeight= null;
		String sortField= null;
		String sortDirection= null;
		
		// When
		MunroControllerBadRequestException thrown = assertThrows(
				MunroControllerBadRequestException.class,
		           () ->  target.munroFilter(limitBy, hillCategory, maxHeight, minHeight, sortField, sortDirection),
		           "Expected munroFilter() to throw an MunroControllerBadRequestException"
		    );
		// Then
		assertThat(thrown).hasMessage("Height FR is not a valid number");
		
	}
	
	@Test
	void shouldFailToCallCallServiceGivenInvalidMinHeight () {
		// Given
		Integer limitBy = null;
		String hillCategory = null;
		String maxHeight= null;
		String minHeight= "XR";
		String sortField= null;
		String sortDirection= null;
		
		// When
		MunroControllerBadRequestException thrown = assertThrows(
				MunroControllerBadRequestException.class,
		           () ->  target.munroFilter(limitBy, hillCategory, maxHeight, minHeight, sortField, sortDirection),
		           "Expected munroFilter() to throw an MunroControllerBadRequestException"
		    );
		// Then
		assertThat(thrown).hasMessage("Height XR is not a valid number");
	}
	
	
	@Test
	void shouldFailToCallCallServiceGivenMinHeightLargerThanMaxHeight () {
		// Given
		Integer limitBy = null;
		String hillCategory = null;
		String maxHeight= "10";
		String minHeight= "15";
		String sortField= null;
		String sortDirection= null;
		
		// When
		MunroControllerBadRequestException thrown = assertThrows(
				MunroControllerBadRequestException.class,
		           () ->  target.munroFilter(limitBy, hillCategory, maxHeight, minHeight, sortField, sortDirection),
		           "Expected munroFilter() to throw an MunroControllerBadRequestException"
		    );
		// Then
		assertThat(thrown).hasMessage("Minimum height must be less than the maximum height");
	}
	
	
	@Test
	void shouldFailToCallCallServiceGivenInvalidSortDirection () {
		// Given
		Integer limitBy = null;
		String hillCategory = null;
		String maxHeight= null;
		String minHeight= null;
		String sortField= "height";
		String sortDirection= "UP";
		
		// When
		MunroControllerBadRequestException thrown = assertThrows(
				MunroControllerBadRequestException.class,
		           () ->  target.munroFilter(limitBy, hillCategory, maxHeight, minHeight, sortField, sortDirection),
		           "Expected munroFilter() to throw an MunroControllerBadRequestException"
		    );
		// Then
		assertThat(thrown).hasMessage("Direction type UP is invalid - valid types are [ASC, DESC]");
		
	}
}
