package com.xdesign.munro.integration;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.xdesign.munro.dto.Munro;

/**
 * Integration tests  
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class MunroIntegrationTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void shouldFetchData() throws Exception {
		// Given
		String url = "http://localhost:" + port + "/api/munro";
		
		// When
		Munro[] result = restTemplate.getForObject(url ,Munro[].class);

		// Then 
		assertThat(result).hasAtLeastOneElementOfType(Munro.class);
	}
	
	
	@Test
	void shouldHandleParamaterError() throws Exception {
		// Given
		String url = "http://localhost:" + port + "/api/munro?max-height=10&min-height=20";
		
		// When
		ResponseEntity<String> result = restTemplate.getForEntity(url ,String.class);

		// Then 
		assertThat(result).hasFieldOrPropertyWithValue("status", HttpStatus.BAD_REQUEST.value()).hasFieldOrPropertyWithValue("body", "Minimum height must be less than the maximum height");
	}
	

	@Test
	void shouldHandleInvalidFieldError() throws Exception {
		// Given
		String url = "http://localhost:" + port + "/api/munro?sort-field=XXX";
		
		// When
		ResponseEntity<String> result = restTemplate.getForEntity(url ,String.class);

		// Then 
		assertThat(result).hasFieldOrPropertyWithValue("status", HttpStatus.BAD_REQUEST.value()).hasFieldOrPropertyWithValue("body", "Sort field XXX is invalid - valid types are [HEIGHT, NAME]");
	}

	
	
}
