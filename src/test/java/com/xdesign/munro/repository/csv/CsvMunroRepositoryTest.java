package com.xdesign.munro.repository.csv;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.xdesign.munro.dto.MunroType;
import com.xdesign.munro.entity.MunroEntity;
import com.xdesign.munro.repository.InvalidFieldException;
import com.xdesign.munro.repository.MunroRepositoryException;

/**
 * unit test class for {@link CsvMunroRepository}
 *
 */
class CsvMunroRepositoryTest {

	private static final String FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE = "Failed to open data source";
	ResourceLoader resourceLoader = new DefaultResourceLoader();

	@Test
	void shouldLoadData() {
		// Given
		Resource resource = givenTestResource("valid-records.csv");
		CsvMunroRepository target = new CsvMunroRepository(resource);
		MunroEntity expectedEntity = MunroEntity.builder().withIndex(1L).withDoBIH(1L).withGridRef("NN773308")
				.withHeight(new BigDecimal("931")).withMunroType(MunroType.MUN).withName("Ben Chonzie").build();

		// When
		Set<MunroEntity> results = target.findAll();

		// Then
		assertThat(results).hasSize(1).contains(expectedEntity);
	}
	
	@Test
	void shouldIgnoretLoadBlankLinesLoadData() {
		// Given
		Resource resource = givenTestResource("valid-records-blank-lines.csv");
		CsvMunroRepository target = new CsvMunroRepository(resource);

		// When
		Set<MunroEntity> results = target.findAll();

		// Then
		assertThat(results).extracting("index").contains(1L, 2L, 3L);
	}

	@Test
	void shouldNotLoadDataGivenInvalidResource() throws IOException {
		// Given
		Resource resource = Mockito.mock(Resource.class);
		IOException expectedRooException = new IOException("test");
		Mockito.when(resource.getInputStream()).thenThrow(expectedRooException);
		CsvMunroRepository target = new CsvMunroRepository(resource);

		MunroRepositoryException thrown = assertThrows(
				MunroRepositoryException.class,
		           () ->  target.findAll(),
		           "Expected findAll() to throw an IOException"
		    );
		
		// Then
		assertThat(thrown).hasCause(expectedRooException) 
		.hasMessageContaining(FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE);
	}
	
	@Test
	void shouldNotLoadDataGivenInvalidIdField() throws IOException {
		// Given
		Resource resource = givenTestResource("invalid-field-id.csv");
		CsvMunroRepository target = new CsvMunroRepository(resource);
		
		MunroRepositoryException thrown = assertThrows(
				MunroRepositoryException.class,
		           () ->  target.findAll(),
		           "Expected findAll() to throw an IOException"
		    );
		
		// Then
		assertThat(thrown).hasCauseInstanceOf(InvalidFieldException.class).hasMessageContaining(FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE);
		assertThat(thrown.getCause()).hasMessageContaining(("Invalid format of field at row 1 , field [Running No] value [X]"));
	}
	
	@Test
	void shouldNotLoadDataGivenInvalidDobihField() throws IOException {
		// Given
		Resource resource = givenTestResource("invalid-field-dobih.csv");
		CsvMunroRepository target = new CsvMunroRepository(resource);
		
		MunroRepositoryException thrown = assertThrows(
				MunroRepositoryException.class,
		           () ->  target.findAll(),
		           "Expected findAll() to throw an IOException"
		    );
		
		// Then
		assertThat(thrown).hasCauseInstanceOf(InvalidFieldException.class).hasMessageContaining(FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE);
		assertThat(thrown.getCause()).hasMessageContaining(("Invalid format of field at row 1 , field [DoBIH Number] value [X]"));
	}
	
	@Test
	void shouldNotLoadDataGivenInvalidHeightField() throws IOException {
		// Given
		Resource resource = givenTestResource("invalid-field-height.csv");
		CsvMunroRepository target = new CsvMunroRepository(resource);
		
		MunroRepositoryException thrown = assertThrows(
				MunroRepositoryException.class,
		           () ->  target.findAll(),
		           "Expected findAll() to throw an IOException"
		    );
		
		// Then
		assertThat(thrown).hasCauseInstanceOf(InvalidFieldException.class).hasMessageContaining(FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE);
		assertThat(thrown.getCause()).hasMessageContaining(("Invalid format of field at row 1 , field [Height (m)] value [XXX]"));
	}
	
	@Test
	void shouldNotLoadDataGivenInvalidTypeField() throws IOException {
		// Given
		Resource resource = givenTestResource("invalid-field-munrotype.csv");
		CsvMunroRepository target = new CsvMunroRepository(resource);
		
		MunroRepositoryException thrown = assertThrows(
				MunroRepositoryException.class,
		           () ->  target.findAll(),
		           "Expected findAll() to throw an IOException"
		    );
		
		// Then
		assertThat(thrown).hasCauseInstanceOf(InvalidFieldException.class).hasMessageContaining(FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE);
		assertThat(thrown.getCause()).hasMessageContaining(("Invalid format of field at row 1 , field [Post 1997] value [XXX]"));
	}
	
	
	@Test
	void shouldNotLoadDataGivenMissingHeaders() throws IOException {
		// Given
		Resource resource = givenTestResource("missing-headers.csv");
		CsvMunroRepository target = new CsvMunroRepository(resource);
		
		MunroRepositoryException thrown = assertThrows(
				MunroRepositoryException.class,
		           () ->  target.findAll(),
		           "Expected findAll() to throw an IOException"
		    );
		
		// Then
		assertThat(thrown).hasCauseInstanceOf(IllegalArgumentException.class).hasMessageContaining(FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE);
	}
	
	@Test
	void shouldNotLoadDataGivenMissingFields() throws IOException {
		// Given
		Resource resource = givenTestResource("invalid-record-missing-fields.csv");
		CsvMunroRepository target = new CsvMunroRepository(resource);
		
		MunroRepositoryException thrown = assertThrows(
				MunroRepositoryException.class,
		           () ->  target.findAll(),
		           "Expected findAll() to throw an IOException"
		    );
		
		// Then
		assertThat(thrown).hasCauseInstanceOf(IllegalArgumentException.class).hasMessageContaining(FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE);
	}
	
	@Test
	void shouldCloseResourceAfterUseStream() throws IOException {
		// Given
		Resource resource = Mockito.mock(Resource.class);
		InputStream inputStream = Mockito.mock(InputStream.class);
		Mockito.when(resource.getInputStream()).thenReturn(inputStream);
		
		CsvMunroRepository target = new CsvMunroRepository(resource);
		
		// When
		MunroRepositoryException thrown = assertThrows(
				MunroRepositoryException.class,
		           () ->  target.findAll(),
		           "Expected findAll() to throw, but it didn't"
		    );
		
		// Then
		assertThat(thrown).hasMessageContaining(FAILED_TO_OPEN_DATA_SOURCE_STD_MESSAGE);
		Mockito.verify(inputStream).close();
	
	}
	

	private Resource givenTestResource (String name) {
		return resourceLoader.getResource("classpath:com/xdesign/munro/repository/CsvMunroRepositoryTest/" + name);
	}
	
	
	
}
