package com.xdesign.munro.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import com.xdesign.munro.repository.MunroRepository;
import com.xdesign.munro.repository.csv.CsvMunroRepository;
import com.xdesign.munro.service.MunroService;
import com.xdesign.munro.service.RepositoryMunroService;

/**
 * Application configuration  
 */
@Configuration
public class MunroConfiguration {
	 
	@Value("classpath:data/munrotab_v6.2.csv")
	private Resource munroDataResource;
	
	@Bean
	public MunroRepository munroRepository() {
		return new CsvMunroRepository(munroDataResource);
	}
	
	@Bean
	public MunroService munroService(MunroRepository munroRepository) {
		return new RepositoryMunroService(munroRepository);
	}

	
	
}
