package com.xdesign.munro.service;

/**
 * A top level exception to throw from with the  MunroService when processing data
 *
 */
public class MunroServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MunroServiceException (String error , Exception root) {
		super(error,root);
	}
	
}
