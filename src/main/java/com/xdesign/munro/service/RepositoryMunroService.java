package com.xdesign.munro.service;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import com.xdesign.munro.dto.Direction;
import com.xdesign.munro.dto.Munro;
import com.xdesign.munro.dto.QueryRequest;
import com.xdesign.munro.dto.Sort;
import com.xdesign.munro.dto.SortField;
import com.xdesign.munro.entity.MunroEntity;
import com.xdesign.munro.repository.MunroRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * implementation of the {@link MunroService}
 *
 */
@Slf4j
public class RepositoryMunroService implements MunroService {

	private final MunroRepository munroRepository;

	private Set<MunroEntity> data;
	
	/**
	 * map of fields and associated {@link Comparator} that can be search on
	 */
	private static final Map<SortField, Comparator<MunroEntity>> FIELD_COMPARATOR_MAP =  Collections.unmodifiableMap(Stream.of(
					new SimpleEntry<>(SortField.HEIGHT, Comparator.comparing(MunroEntity::getHeight)),
					new SimpleEntry<>(SortField.NAME, Comparator.comparing(MunroEntity::getName)))
			.collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue)));

	/**
	 * constructor 
	 * @param munroRepository the munroRepository to load data from
	 */
	public RepositoryMunroService(MunroRepository munroRepository) {
		this.munroRepository = munroRepository;
	}

	@PostConstruct
	public void preloadData() {
		data = munroRepository.findAll();
		log.info("Loaded {} records",data.size());
	}

	@Override
	public List<Munro> searchMunroData(QueryRequest request) {
		
		log.trace("search data with {}" ,request);
		
		Predicate<MunroEntity> filterPredicate = compilefilterPredicate(request);
		Comparator<MunroEntity> munroEntityComparator = compileMunroEntityComparator(request);
		Integer limit = Optional.ofNullable(request.getLimit()).orElse(data.size());
		
		return data.stream()
				.filter(filterPredicate)
				.sorted(munroEntityComparator)
				.map(this::convert)
				.limit(limit)
				.collect(Collectors.toList());

	}

	private Predicate<MunroEntity> compilefilterPredicate(QueryRequest request) {
		
		Predicate<MunroEntity> filterPredicate = itm -> itm.getMunroType() != null;

		if (request.getHillCategory() != null) {
			filterPredicate = filterPredicate.and(itm -> itm.getMunroType() == request.getHillCategory());
		}

		if (request.getMaxHeight() != null) {
			filterPredicate = filterPredicate.and(itm -> itm.getHeight().compareTo(request.getMaxHeight()) <= 0);
		}

		if (request.getMinHeight() != null) {
			filterPredicate = filterPredicate.and(itm -> itm.getHeight().compareTo(request.getMinHeight()) >= 0);
		}

		return filterPredicate;
	}



	private Comparator<MunroEntity> compileMunroEntityComparator(QueryRequest request) {
		Comparator<MunroEntity> munroEntityComparator = null;

		if (request.getSort() != null) {
			for (Sort sort : request.getSort()) {

				if (!FIELD_COMPARATOR_MAP.containsKey(sort.getField())) {
					throw new MunroServiceException("Unable to sort on field " + sort.getField().name(), null);
				}

				Comparator<MunroEntity> foundComparator = FIELD_COMPARATOR_MAP.get(sort.getField());

				if (sort.getDirection() == Direction.DESC) {
					foundComparator = foundComparator.reversed();
				}

				if (munroEntityComparator == null) {
					munroEntityComparator = foundComparator;
				} else {
					munroEntityComparator = munroEntityComparator.thenComparing(foundComparator);
				}
			}
		}

		if (munroEntityComparator == null) {
			munroEntityComparator = Comparator.comparing(MunroEntity::getIndex);
		}
		return munroEntityComparator;
	}
	
	private Munro convert(MunroEntity source) {
		return Munro.builder().withName(source.getName()).withGridRef(source.getGridRef())
				.withHeight(source.getHeight()).withMunroType(source.getMunroType()).build();
	}

}
