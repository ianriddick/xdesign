package com.xdesign.munro.service;

import java.util.List;

import com.xdesign.munro.dto.Munro;
import com.xdesign.munro.dto.QueryRequest;

/**
 * interface for searching Munro Data
 *
 */
public interface MunroService {

	/**
	 * query the munro data 
	 * @param request - specify search and sort parameters
	 * @return list filtered and sorted data - or an empty list if none found
	 */
	List<Munro> searchMunroData (QueryRequest request);	
	
}
