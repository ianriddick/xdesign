package com.xdesign.munro.repository.csv;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;

import com.xdesign.munro.dto.MunroType;
import com.xdesign.munro.entity.MunroEntity;
import com.xdesign.munro.repository.InvalidFieldException;
import com.xdesign.munro.repository.MunroRepository;
import com.xdesign.munro.repository.MunroRepositoryException;

import lombok.extern.slf4j.Slf4j;

/**
 * CSV implementation of the {@link MunroRepository }
 */
@Slf4j
public class CsvMunroRepository implements MunroRepository {

	/**
	 * field names as the appear in the CSV file
	 */
	private static final String TYPE_FIELD_NAME = "Post 1997";
	
	private static final String HEIGHT_FIELD_NAME = "Height (m)";
	
	private static final String DO_BIH_FIELD_NAME = "DoBIH Number";
	
	private static final String GRID_REF_FIELD_NAME = "Grid Ref";
	
	private static final String ID_FIELD_NAME = "Running No";
	
	private static final String NAME_FIELD_NAME = "Name";
	
	
	private final Resource dataSource;

	public CsvMunroRepository(Resource dataSource) {
		log.debug("construct Repository with {} ",dataSource);
		this.dataSource = dataSource;
	}

	@Override
	public Set<MunroEntity> findAll() {
		Set<MunroEntity> result = new HashSet<>();
		try (final Reader reader = new InputStreamReader(dataSource.getInputStream())) {
			Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(reader);

			for (CSVRecord record : records) {
				
				log.trace("reading record {}", record);
				
				if (extractLong(record, ID_FIELD_NAME) != null) {

					MunroEntity entity = MunroEntity.builder().withIndex(extractLong(record, ID_FIELD_NAME))
							.withDoBIH(extractLong(record, DO_BIH_FIELD_NAME))
							.withGridRef(extractString(record, GRID_REF_FIELD_NAME))
							.withHeight(extractBigDeciaml(record, HEIGHT_FIELD_NAME))
							.withMunroType(extractMunroType(record, TYPE_FIELD_NAME))
							.withName(extractString(record, NAME_FIELD_NAME)).build();
					result.add(entity);
				} else {
					log.warn("ignore blank line {}", record.getRecordNumber() );
				}
			}

		} catch (IOException | IllegalArgumentException | InvalidFieldException e) {
			throw new MunroRepositoryException("Failed to open data source ", e);
		}

		return result;
	}

	private Long extractLong(CSVRecord record, String fieldName) {
		String data = extractString(record, fieldName);
		if (StringUtils.isBlank(data)) {
			return null;
		}
		try {
			return Long.parseLong(data);
		} catch (NumberFormatException e) {
			throw new InvalidFieldException(record.getRecordNumber(), fieldName, data, e);
		}
	}

	private BigDecimal extractBigDeciaml(CSVRecord record, String fieldName) {
		String data = extractString(record, fieldName);
		if (StringUtils.isBlank(data)) {
			return null;
		}
		try {
			return new BigDecimal(data);
		} catch (NumberFormatException e) {
			throw new InvalidFieldException(record.getRecordNumber(), fieldName, data, e);
		}
	}

	private MunroType extractMunroType(CSVRecord record, String fieldName) {
		String data = extractString(record, fieldName);
		if (StringUtils.isBlank(data)) {
			return null;
		}
		try {
			return MunroType.valueOf(data);
		} catch (IllegalArgumentException e) {
			throw new InvalidFieldException(record.getRecordNumber(), fieldName, data, e);
		}
	}

	private String extractString(CSVRecord record, String fieldName) {
		return record.get(fieldName);
	}

}
