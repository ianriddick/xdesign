package com.xdesign.munro.repository;

/**
 * An exception to throw if a field cannot be loaded  
 */
public class InvalidFieldException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidFieldException (long rowIndex, String field, String value , Exception root) {
		super(formatMessage(rowIndex,field,value),root);
	}
	
	private static String formatMessage (long rowIndex, String field, String value) {
		return String.format("Invalid format of field at row %s , field [%s] value [%s]", rowIndex,field, value);
	}
}
