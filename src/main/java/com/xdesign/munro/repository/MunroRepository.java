package com.xdesign.munro.repository;

import java.util.Set;

import com.xdesign.munro.entity.MunroEntity;

/**
 * interface for the Munro Repository
 *
 */
public interface MunroRepository {

	/**
	 * return a set of {@link MunroEntity } stored in the Repository
	 * @return all data or empty if non are found
	 */
	public Set<MunroEntity> findAll ();
	
}
