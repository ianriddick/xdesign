package com.xdesign.munro.repository;

/**
 * A top level exception to throw from with the  MunroRepository when processing data
 *
 */
public class MunroRepositoryException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MunroRepositoryException (String error , Exception root) {
		super(error,root);
	}
	
}
