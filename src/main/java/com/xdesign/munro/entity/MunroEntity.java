package com.xdesign.munro.entity;

import java.math.BigDecimal;

import com.xdesign.munro.dto.MunroType;

import lombok.Builder;
import lombok.Data;

/**
 * Entity class to hold information loaded from the data source 
 */
@Builder(setterPrefix = "with")
@Data
public class MunroEntity {

	/**
	 * the data index
	 */
	private Long index;
	
	/**
	 * stable hill reference
	 */
	private Long doBIH;

	/**
	 * munro name
	 */
	private String name;

	/**
	 * height in M
	 */
	private BigDecimal height;

	/**
	 * grid reference
	 * 
	 * format of NN773308
	 */
	private String gridRef;

	/**
	 * the type of munro either MUN or TOP null
	 */
	private MunroType munroType;

}
