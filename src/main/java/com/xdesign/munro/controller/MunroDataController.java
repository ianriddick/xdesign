package com.xdesign.munro.controller;

import static  java.util.Optional.ofNullable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xdesign.munro.controller.exception.MunroControllerBadRequestException;
import com.xdesign.munro.dto.Direction;
import com.xdesign.munro.dto.Munro;
import com.xdesign.munro.dto.MunroType;
import com.xdesign.munro.dto.QueryRequest;
import com.xdesign.munro.dto.Sort;
import com.xdesign.munro.dto.SortField;
import com.xdesign.munro.service.MunroService;

/**
 * The Munro Data Controller 
 */
@RestController
@RequestMapping("/api")
public class MunroDataController {
	

	private MunroService munroService;
	
	/**
	 * constructor 
	 * @param munroService the munro service for accessing data
	 */
	public MunroDataController (MunroService munroService) {
		this.munroService = munroService;
	}
	
	/**
	 * filter and order the munro data 
	 * @param limitBy limit the results returned - this is applied after the filter and sorting has been applied to return the top x
	 * @param hillCategory the hill category to filter on - value must map to {@link MunroType}
	 * @param maxHeight return all records with a height lower than this value
	 * @param minHeight return all records with a height greater than this value
	 * @param sortField the single field to sort the record on - value must map to {@link SortField}
	 * @param sortDirection the direction of sort - value must map to {@link Direction}
	 * @return
	 */
	@GetMapping("/munro")
    public List<Munro> munroFilter(
    			@RequestParam(name = "max-records", required = false)  Integer limitBy,
      			@RequestParam(name = "hill-category", required = false) String hillCategory,
      			@RequestParam(name = "max-height", required = false)  String maxHeight,
      			@RequestParam(name = "min-height", required = false)  String minHeight,
      			@RequestParam(name = "sort-field", required = false)  String sortField,
      			@RequestParam(name = "sort-direction", required = false)  String sortDirection
    		) {

		List<Sort> sort = new ArrayList<>();

		ofNullable(sortField).ifPresent( sf -> 
			sort.add(Sort.builder().withField(convertSortFieldType(sf)).withDirection(convertDirectionValue(ofNullable(sortDirection).orElse("ASC"))).build())
		);
		
		QueryRequest request = QueryRequest.builder()
				.withLimit(limitBy)
				.withHillCategory(convertMunroType(hillCategory))
				.withMaxHeight(convertHeightValue(maxHeight))
				.withMinHeight(convertHeightValue(minHeight))
				.withSort(sort)
				.build();
		
		assertUserRequest(request);
        return munroService.searchMunroData(request);
    }

	/**
	 * exception handler to handle MunroControllerBadRequestException
	 * @param ex the thrown exception
	 * @return an error message
	 */
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MunroControllerBadRequestException.class)
	@ResponseBody
	public String handleAllOtherException(MunroControllerBadRequestException ex) {
		return ex.getMessage();
	}
	
	private void assertUserRequest(QueryRequest request) {
		if ((request.getMinHeight() != null) && (request.getMaxHeight() != null)) {
			if (request.getMinHeight().compareTo(request.getMaxHeight()) >= 0) {
				throw new MunroControllerBadRequestException("Minimum height must be less than the maximum height");
			}
		}
	}

	private MunroType convertMunroType(String value) {
		if (value == null) {
			return null;
		}
		try {
			return MunroType.valueOf(value);
		} catch (IllegalArgumentException e) {
			String message = String.format("Munro type %s is invalid - valid types are %s", value,
					Arrays.asList(MunroType.values()));
			throw new MunroControllerBadRequestException(message);
		}

	}

	private SortField convertSortFieldType(String value) {
		if (value == null) {
			return null;
		}
		try {
			return SortField.valueOf(value.toUpperCase());
		} catch (IllegalArgumentException e) {
			String message = String.format("Sort field %s is invalid - valid types are %s", value,
					Arrays.asList(SortField.values()));
			throw new MunroControllerBadRequestException(message);
		}

	}

	private BigDecimal convertHeightValue(String value) {
		if (value == null) {
			return null;
		}
		try {
			return new BigDecimal(value);
		} catch (IllegalArgumentException e) {
			String message = String.format("Height %s is not a valid number", value);
			throw new MunroControllerBadRequestException(message);
		}
	}

	private Direction convertDirectionValue(String value) {
		if (value == null) {
			return null;
		}
		try {
			return Direction.valueOf(value);
		} catch (IllegalArgumentException e) {
			String message = String.format("Direction type %s is invalid - valid types are %s", value,
					Arrays.asList(Direction.values()));
			throw new MunroControllerBadRequestException(message);
		}
	}
	
}
