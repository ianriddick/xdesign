package com.xdesign.munro.controller.exception;

/**
 * Exception class to be thrown on invalid input parameters 
 */
public class MunroControllerBadRequestException extends RuntimeException {
   
	private static final long serialVersionUID = 1L;

	public MunroControllerBadRequestException(String reason) {
    	super(reason);
    }

}
