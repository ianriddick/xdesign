package com.xdesign.munro.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Query Request DTO to hold filter and query parameters
 */
@Builder(setterPrefix = "with")
@Data
@ToString
public class QueryRequest {

	private MunroType hillCategory;
	
	private BigDecimal minHeight;
	
	private BigDecimal maxHeight;

	private List<Sort> sort;

	private Integer limit;

}
