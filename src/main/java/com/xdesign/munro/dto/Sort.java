package com.xdesign.munro.dto;

import lombok.Builder;
import lombok.Data;

@Builder(setterPrefix = "with")
@Data
public class Sort {
		private SortField field;
		private Direction direction;
}
