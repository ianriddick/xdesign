package com.xdesign.munro.dto;

/**
 * Represent the type of hill 
 */
public enum MunroType {
	MUN,
	TOP;
}
