package com.xdesign.munro.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * Data transfer object to represent the Munro data
 *
 */
@Builder(setterPrefix = "with")
@Data
@ToString
public class Munro {

	/**
	 * munro name
	 */
	private String name;

	/**
	 * height in M
	 */
	private BigDecimal height;

	/**
	 * grid reference
	 * 
	 * format of NN773308
	 */
	private String gridRef;

	/**
	 * the type of munro either MUN or TOP null
	 */
	private MunroType munroType;

}
