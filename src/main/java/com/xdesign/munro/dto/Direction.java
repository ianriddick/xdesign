package com.xdesign.munro.dto;

/**
 * represent the sort direction 
 */
public enum Direction {
	ASC ,
	DESC 
}
