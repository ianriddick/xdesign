package com.xdesign.munro.dto;

/**
 * Represent the fields that can be sorted 
 */
public enum SortField {
	HEIGHT ,
	NAME 
}
